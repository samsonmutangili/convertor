package com.example.convertor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText millimeters_input = findViewById(R.id.millimeter_edit_text);
        final TextView inches_output = findViewById(R.id.inches_text_view);

        Button convert = findViewById(R.id.convert_button);

        Button exit_button = findViewById(R.id.exit_button);

        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //get the user input
                String millimeters = millimeters_input.getText().toString();

                if(millimeters == null){
                    Toast.makeText(MainActivity.this, "Error!! Input is empty!!", Toast.LENGTH_SHORT).show();
                }
                else{

                    try{
                        //get millimeters and convert user input into double format
                        double millimeters_int = Double.parseDouble(millimeters);

                        //conversion formula
                        //inches = millimeters/25.4

                        DecimalFormat df = new DecimalFormat("#.###");

                        //convert
                        double inches = (millimeters_int/25.4);
                        String standard_inches = df.format(inches);

                        inches_output.setText(standard_inches);

                    } catch (Exception ex){
                        Toast.makeText(MainActivity.this, "Error!! "+ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        exit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
